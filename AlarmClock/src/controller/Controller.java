package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Timer;

import application.Main;
import application.IsTimer;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.scene.control.Button;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.TextField;

public class Controller {
	
	public String pathMusic = null;
	private int interval = 0;
	
	@FXML
	TextField HourField;
	@FXML
	TextField MinuteField;
	@FXML
	Button stopButton;
	@FXML
	RadioMenuItem min1;
	@FXML
	RadioMenuItem min5;
	@FXML
	RadioMenuItem min10;
	@FXML
	RadioMenuItem min30;
	@FXML
	TextField MonthField;
	@FXML
	TextField DayField;
	Calendar c;
	Alert alert;
	Timer timer;
	IsTimer obj;
	@FXML
	public void initialize(){
		c = Calendar.getInstance();
		setDefaultData(c);
	}
	@FXML
	public void startTime() throws IOException{
		pathMusic = getPathMusic(pathMusic);
		obj = new IsTimer(pathMusic);
		if(pathMusic != null) {
			try{
				c.set(c.get(Calendar.YEAR), Integer.parseInt(MonthField.getText())-1,
					Integer.parseInt(DayField.getText()), Integer.parseInt(HourField.getText()), 
					Integer.parseInt(MinuteField.getText()), 0);
				stopButton.setVisible(true);
				timer = new Timer("To Started");
				timer.scheduleAtFixedRate(obj, c.getTime(), interval);
			}catch(NumberFormatException e){
				isError();
			}catch(IllegalArgumentException ia){
				timer.schedule(obj, c.getTime());
			}
		} else {
			alert = new Alert(AlertType.ERROR);
			alert.setHeaderText("�� �������� �������� ������");
			alert.setContentText("�������� �������� ������ ����������");
			alert.showAndWait();
		}
	}
	@FXML
	public void cancelTime(){
		timer.cancel();
		obj.stopMusicAndContinue();
		stopButton.setVisible(false);
	}
	//TODO Button StopMusic
	private String getPathMusic(String aPath){
		try{
			BufferedReader reader = new BufferedReader(new FileReader("./src/SourceMusic.txt"));
			aPath = reader.readLine();
			reader.close();
		}catch(IOException ioe){
			return null;
		}
		return aPath;
	}
	private void isError(){
		alert = new Alert(AlertType.ERROR);
		alert.setHeaderText("������� ��������� ������ �������");
		alert.setContentText("� ����� ���������� �������, ���������� �� ������");
		alert.showAndWait();
	}
	@FXML
	public void getIntervalOne(){
		if(min1.isSelected()){
			interval = 60000;
			min5.setSelected(false);
			min10.setSelected(false);
			min30.setSelected(false);
		}
	}
	@FXML
	public void getIntervalFive(){
		if(min5.isSelected()){
			interval = 60000*5;
			min1.setSelected(false);
			min10.setSelected(false);
			min30.setSelected(false);
		}
	}
	@FXML
	public void getIntervalTen(){
		if(min10.isSelected()){
			interval = 60000*10;
			min1.setSelected(false);
			min5.setSelected(false);
			min30.setSelected(false);
		}
	}
	@FXML
	public void getIntervalThirty(){
		if(min30.isSelected()){
			interval = 60000*30;
			min1.setSelected(false);
			min5.setSelected(false);
			min10.setSelected(false);
		}
	}
	private void setDefaultData(Calendar c){
		MonthField.setText(String.valueOf(c.get(Calendar.MONTH)+1));
		DayField.setText(String.valueOf(c.get(Calendar.DAY_OF_MONTH)));
	}
	public void onSourceMusic() {
		Main main = new Main();
		main.getClass();
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Music");
		FileChooser.ExtensionFilter extFilter =  new FileChooser.ExtensionFilter("Music file (*.mp3)","*.mp3");
		fileChooser.getExtensionFilters().add(extFilter);
		File file = fileChooser.showOpenDialog(main.primaryStage);
		try {
			pathMusic = file.getAbsolutePath();
			if(pathMusic != null) {
				FileWriter writer = new FileWriter("./src/SourceMusic.txt", false);
				writer.write(pathMusic);
				writer.flush();
				writer.close();
			}
		}catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}catch(NullPointerException npe){
		}
	}
}
