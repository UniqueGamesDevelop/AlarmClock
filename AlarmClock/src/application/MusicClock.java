package application;

import java.io.*;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

public class MusicClock implements Runnable {

	private String linkMusic = null;
	Player player;
	public MusicClock(String aSourceMusic) {
			linkMusic = aSourceMusic;
	}
	
	@Override
	public void run() {
		FileInputStream file = null;
		try {
			file = new FileInputStream(linkMusic);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			player = new Player(file);
			player.play();
		} catch (JavaLayerException e) {
			e.printStackTrace();
		}
	}
	
	public Player getPlayer(){
		return player;
	}
}
