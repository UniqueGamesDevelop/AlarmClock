	/*
	 * 1) �������� ���, ������ ��� ����
	 * 2) Tested by Desperando
	 */
package application;

import java.util.TimerTask;

public class IsTimer extends TimerTask{
	Thread tMusic;
	MusicClock mc;
	String sourceMusic;
	public IsTimer(String aSourceMusic){
		sourceMusic = aSourceMusic;
	}
	public void run(){
		if(mc!=null)
			stopMusicAndContinue();
		mc = new MusicClock(sourceMusic);
		tMusic = new Thread(mc);
		tMusic.start();
	}
	public void stopMusicAndContinue(){
		mc.getPlayer().close();
	}
	public Thread getTMusic() {
		return tMusic;
	}
}
